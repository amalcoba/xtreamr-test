import React, { Component } from 'react';
import '../styles/PopularMovies.css';

export class PopularMovies extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			movies:[],
			isLoaded: false,
			error: null
		}

		this.getData = this.getData.bind(this);		
	}
	


	getData() {
		const key = 'c5e4a1733c2995102fafe209c014e4c0';
	    

	    fetch(`https://api.themoviedb.org/3/discover/movie?api_key=c5e4a1733c2995102fafe209c014e4c0&language=es-ES&sort_by=popularity.desc&include_adult=false&include_video=false&page=1`)
			.then(res => res.json())
	      	.then(
	      		(data) => {
					this.setState({
						isLoaded: true,
						movies: data.results
					});
	      		},
	      		(error) => {
	      			this.setState({
						isLoaded: true,
						error: error
	      			});      
	      			console.log('Error:' + error);		
	      		}        

	    	)	
	}

	componentDidMount() {
		this.getData();		

	}

	render() {
		return(
			<div className="moviesContainer">		
		        <h2>Popular Movies</h2>
		        <div className="moviesList">		        
		          {this.state.movies.map((movie, index) => {
		            return (
						<div className="movieCard">
							<a href={`/movie/${this.state.movies[index].id}`}>
			            		<img src={`https://image.tmdb.org/t/p/w300/${this.state.movies[index].poster_path}`} />
			            	</a>		            					            			            	
			                <div className="movieDetails">
			                  <h3>{this.state.movies[index].title}</h3>
			                  <p>{this.state.movies[index].release_date}</p>			                  
			                </div>

		                </div>		              
		            )
		          })}
		          </div>
		        
	        </div>      
		);
	}
}

import React, { Component } from 'react';
import '../styles/Header.css';

export class Header extends Component {
  render() {
    return(
      <header>
      	<h1>My MoviesDB</h1>
		<div className="PageName" />
		<div className="logo" />
      </header>
    );
  }
}
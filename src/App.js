import React, { Component } from 'react';
import './App.css';
import  {PopularMovies} from './components/PopularMovies'
import {Header} from './components/Header';
import {Footer} from './components/Footer';

class App extends Component {
  render() {
    return (
      <div>
      	<Header />
        <PopularMovies />
        <Footer />
      </div>
    );
  }
}

export default App;
